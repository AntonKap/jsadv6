"use strict"
document.getElementById('findIp').addEventListener('click', async function() {
    try {
        let ipResponse = await fetch('https://api.ipify.org/?format=json');
        let ipData = await ipResponse.json();
        let geoResFilds = await fetch(`http://ip-api.com/json/${ipData.ip}`);
        let geoData = await geoResFilds.json();
        let { timezone, country, regionName, city, zip } = geoData;
        let InfoElement = document.getElementById('ipInfo');
        InfoElement.insertAdjacentHTML("beforeend", `
            <p><strong>ip:</strong> ${ipData.ip}</p>
            <p><strong>Континент:</strong> ${timezone}</p>
            <p><strong>Країна:</strong> ${country}</p>
            <p><strong>Регіон:</strong> ${regionName}</p>
            <p><strong>Місто:</strong> ${city}</p>
            <p><strong>Район:</strong> ${zip}</p>
        `);
    } catch (error) {
        console.error(error);
    }
});
